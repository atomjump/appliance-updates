<?php

	 $set_video_audio = null;
	 
	 foreach($argv as $entry) {
	 	if($entry == "video") $set_video_audio = true;
	 	if($entry == "videothumbs") $set_video_thumbnails = true;
	 }



		//Update the config file with the latest js version chat-1.1.2.js, which has been downloaded already
		$config_file = "/var/www/html/livewiki/config.php";
		$new_chat_js = "chat-1.1.2.js";
		$full_js_path = "/var/www/html/livewiki/js/" . $new_chat_js;
		
		if(file_exists($full_js_path)) {
		
			$config_str = file_get_contents($config_file);
			if(strpos($config_str, $new_chat_js) != false) {
				//it already exists in the file.
				echo "Note: The chat connector has already been updated to " . $new_chat_js . "\n";
			} else {
				//Add in the chatInnerJSFilename to the config file.
				//$atomjump_js_path = "*" replaced with $atomjump_js_path = "$new_chat_js"
				$regexp = "#atomjump_js_path = \"(.*?)\"#";
				$replacement = "atomjump_js_path = \"js/" . $new_chat_js . "\"";
				$config_str = preg_replace($regexp, $replacement, $config_str); 
							
				//Rewrite the file
				file_put_contents($config_file, $config_str);
				
				echo "Note: The chat connector has already been updated to " . $new_chat_js . "\n";
			}
		} else {
			echo "Warning: we could not find the new chat connector file at " . $full_js_path . "\n";
		}
		
		
		
		//Update the messaging config to include the right objects. Ask if this is an https site, switch on video/audio recordings too.
		
		
		$config_file = "/var/www/html/vendor/atomjump/loop-server/config/config.json";
		$config = json_decode(file_get_contents($config_file), true);
		
		//Add in the resizeVideo tag to the config file.
		 echo "Adding in the resizeVideo tag..\n";
		 $jsonobj = '{
			"url": "https://online-video-cutter.com/[LANG]/resize-video",
			"en": "en",
			"pt" : "pt",
			"it": "it",
			"es" : "es",
			"de": "de",
			"fr": "fr",
			"ru": "ru",
			"jp": "ja",
			"ko": "ko",
			"ch": "cn",
			"cht": "tw",
			"in": "id",
			"ar": "en",
			"bg": "en",
			"hi": "en",
			"pu": "en"							  	  
		  }';
		  $resize_vid = json_decode($jsonobj, true);
		  if(!isset($config['production']['uploads']['resizeVideo'])) {		//only change if not here already
		  	$config['production']['uploads']['resizeVideo'] = $resize_vid;
		  }
		  if(!isset($config['staging']['uploads']['resizeVideo'])) {			//only change if not here already
		  	$config['staging']['uploads']['resizeVideo'] = $resize_vid;
		  }
		  
		 //Add in the recordAudio tag
		  $jsonobj = '{
		  	"url": "https://online-voice-recorder.com/[LANG]/",
		  	"en": "en",
			"pt" : "pt",
			"it": "it",
			"es" : "es",
			"de": "de",
			"fr": "fr",
			"ru": "ru",
			"jp": "ja",
			"ko": "ko",
			"ch": "cn",
			"cht": "tw",
			"in": "id",
			"ar": "en",
			"bg": "en",
			"hi": "en",
			"pu": "en"			  
		  }';
		  $record_aud = json_decode($jsonobj, true);
		  if(!isset($config['production']['uploads']['recordAudio'])) {	//only change if not here already
		  	$config['production']['uploads']['recordAudio'] = $record_aud;
		  }
		  if(!isset($config['staging']['uploads']['recordAudio'])) {	//only change if not here already
		  	$config['staging']['uploads']['recordAudio'] = $record_aud;
		  }
		  
		  //Add in the videoThumbnails
		  if(!isset($config['production']['uploads']['videoThumbnails'])) {	//only change if not here already
		  	$config['production']['uploads']['videoThumbnails'] = false;
		  }
		  if(!isset($config['staging']['uploads']['videoThumbnails'])) {	//only change if not here already
		  	$config['staging']['uploads']['videoThumbnails'] = false;
		  }
		  
		//Add in the recording tags. By default to 			  
		  $jsonobj = '{		
		  	 "supported": true,
		  	 "types": {
		  	 	"video": false,
		  	 	"audio": false,
		  	 	"photo": true
		  	 }			  
		  }';
		  
		  $recording = json_decode($jsonobj, true);	
		  if(!$config['production']['recording']) {			//only change if not here already
		  	$config['production']['recording'] = $recording;
		  }
		  
		  if(!$config['staging']['recording']) {			//only change if not here already
		 	 $config['staging']['recording'] = $recording;
		  }
		
		//User input on whether they want to include live audio/video
		if($set_video_audio == true) {
			
			//And re-update config for full support  	
		  	$config['staging']['recording']['types']['video'] = true;
		  	$config['production']['recording']['types']['video'] = true;
		  	
		  	$config['staging']['recording']['types']['audio'] = true;
		  	$config['production']['recording']['types']['audio'] = true;
		
			$config['production']['uploads']['videoThumbnails'] = true;
		  	$config['staging']['uploads']['videoThumbnails'] = true;
		  	
		  	$config['production']['video']['hostHttps'] = true;
		  	$config['staging']['video']['hostHttps'] = true;
		
		}
		
		//User input on whether they want video thumbnails
		if($set_video_thumbnails == true) {
			$config['production']['uploads']['videoThumbnails'] = true;
		  	$config['staging']['uploads']['videoThumbnails'] = true;
		}

		
		
		//Rewrite the file
		$config_str = json_encode($config, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
		 
		file_put_contents($config_file, $config_str);
		
		echo "Added in the resizeVideo tag.\n";	
		
		
?>
