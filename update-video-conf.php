<?php

	
		
		//Update the messaging config with an overwritten AtomJump-based video conferencing.
		$config_file = "/var/www/html/vendor/atomjump/loop-server/config/config.json";
		$config = json_decode(file_get_contents($config_file), true);
		
		//Add in the resizeVideo tag to the config file.
		 echo "Adding in the AtomJump video conferencing..\n";
		 $jsonobj = '{
			"url": "https://jitsi[SERVERID].atomjump.com/[FORUM]?lang=[LANG]",
			"privateUrl": "https://jitsi[SERVERID].atomjump.com/[FORUM]?lang=[LANG]",
			"domain": "jitsi[SERVERID].atomjump.com",
			"privateDomain": "jitsi[SERVERID].atomjump.com",
			"totalVideoServers": 1,
			"totalVideoServersUrl": "https://atomjump.com/api/vid-server-cnt.php",
			"hostHttps": false,
			"jitsi-codes": "See table here: https://github.com/jitsi/jitsi-meet/blob/master/lang/languages.json",
			"langCodeInnerVsOuter": {
				"en": "en",
				"ch": "zhCN",
				"cht": "zhTW",
				"ar": "ar",
				"bg": "en",
				"de": "de",
				"es" : "es",
				"pt" : "pt",
				"fr": "fr",
				"hi": "hi",
				"in": "id",
				"it": "it",
				"jp": "ja",
				"ko": "ko",
				"pu": "en",
				"ru": "ru"			
			}
		}';
		
		$video_settings = json_decode($jsonobj, true);
		  
		$config['production']['video'] = $video_settings;
		$config['staging']['video'] = $video_settings;
		  		  
		  	
		
		//Rewrite the file
		$config_str = json_encode($config, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
		 
		file_put_contents($config_file, $config_str);
		
		echo "Added in the AtomJump video conferencing.\n";		
		
		
?>
